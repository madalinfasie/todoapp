from django.shortcuts import render
from django.views.generic import ListView

from .models import ToDoEntries


class ToDoList(ListView):
    """ View for To Do entries """
    model = ToDoEntries
    context_object_name = 'todo_entries'
    template_name = 'mainapp/home.html'
