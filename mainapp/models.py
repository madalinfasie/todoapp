from django.db import models

class ToDoEntries(models.Model):
    """ Model for To Do entries """
    title = models.CharField(max_length=250)
    body = models.TextField()
    deadline = models.DateTimeField(null=True, blank=True)
    created_date = models.DateTimeField(blank=True)
    modified_date = models.DateTimeField(blank=True)
    # TODO: add a relationship with steps model (when the latter is created)

    class Meta:
        db_table='ToDoEntries'

class ToDoEntries(models.Model):
    """ Model for To Do entries """
    title = models.CharField(max_length=250)
    body = models.TextField()
    deadline = models.DateTimeField(null=True, blank=True)
    created_date = models.DateTimeField(blank=True)
    modified_date = models.DateTimeField(blank=True)
    # TODO: add a relationship with steps model (when the latter is created)

    class Meta:
        db_table='ToDoEntries'